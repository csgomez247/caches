//Nathan Lin, Corbin Gomez

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <string>
#include <fstream>

#define MEMSIZE 65536 // in bytes
#define NUMWAYS 6
#define NUMSETS 10
#define LINESIZE 4 // in bytes

using namespace std;

void newLine();
int charToHex(unsigned char c);
int extractBits(int a, int b, int n);
int extractBits(int a, int b, long n);
void factorAddress(int address, int & tag, int & setNum, int & offset);
void printB(unsigned long x);
void printB(int x);
void printB(unsigned char x);
void printH(int x);
void printData(unsigned char data);
void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, unsigned char from);
void processInput(string s, unsigned char & tag, int & setNum, int & offset, bool & write, unsigned char & data);
void setBit(unsigned long & data, int pos);
void clearBit(unsigned long & data, int pos);

class Memory;
class Line;
class Set;
class SACache;

void newLine()
{
	cout << endl;
}//newLine

int charToHex(unsigned char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else
		return c - 'A' + 10;
}//unsigned charToHex

int extractBits(int a, int b, int n)
{
	int r = 0;
	for(int i = a; i <= b; i++)
		r |= 1 << i;
	r &= n;
	r >>= a;
	return r;
}//extractBits

int extractBits(int a, int b, unsigned long n)
{
	int r = 0;
	for(int i = a; i <= b; i++)
		r |= 1 << i;
	r &= n;
	r >>= a;
	return r;
}//extractBits

void printB(unsigned long x)
{
	for(int i = 31; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printB(int x)
{
	for(int i = 15; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printB(unsigned char x)
{
	for(int i = 7; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printH(int x)
{
	cout << uppercase << hex << x;
	cout << resetiosflags(ios::uppercase) << resetiosflags(ios::hex);
}

void printData(unsigned char data)
{
	unsigned char a = 0;
	writeBits(0,3,4,7,a,data);
	cout << hex << uppercase << (int)a;
	writeBits(0,3,0,3,a,data);
	cout << hex << uppercase << (int)a;
}//printData

void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, unsigned char from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void writeBits(int toA, int toB, int fromA, int fromB, int & to, unsigned char from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, int from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void writeBits(int toA, int toB, int fromA, int fromB, int & to, int from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void processInput(string s, int & address, bool & write, unsigned char & data)
{
	int input[10];
	for(int i = 0; i < 10; i++)
		input[i] = charToHex(s[i]);

	writeBits(0,3,0,3,address,input[3]);
	writeBits(4,7,0,3,address,input[2]);
	writeBits(8,11,0,3,address,input[1]);
	writeBits(12,15,0,3,address,input[0]);
	
	if(s[5] == 'F' && s[6] == 'F')
		write = true;
	else
		write = false;

	writeBits(4,7,0,3,data,input[8]);
	writeBits(0,3,0,3,data,input[9]);

}//processInput

void factorAddress(int address, int & tag, int & setNum, int & offset)
{
	//write tag
	tag = 0;
	writeBits(0, 13, 2, 15, tag, address);

	//write setNum
	setNum = 0;
	setNum = (address / LINESIZE) % NUMSETS;

	//write offset
	offset = 0;
	writeBits(0, 1, 0, 1, offset, address);
}//factorAddress

void setBit(unsigned long long & data, int pos)
{
	data |= 1 << pos;
}//setBit

void clearBit(unsigned long long & data, int pos)
{
	data &= ~(1 << pos);
}//clearBit

class Memory
{
private:
	unsigned char * mem;
public:
	Memory()
	{
		mem = new unsigned char[MEMSIZE];
		for(int i = 0; i < MEMSIZE; i++)
			mem[i] = 0;
	}//Memory()

	~Memory()
	{
		delete [] mem;
	}//~Memory()

	int computeBlock(int address)
	{
		return (address / LINESIZE) * LINESIZE;
	}//computeBlock

	unsigned char * getBlock(int address)
	{
		return & mem[(address / LINESIZE) * LINESIZE];
	}//getBlock

	unsigned char readMem(int address)
	{
		return mem[address];
	}//read

	void writeMem(int address, unsigned char input)
	{
		mem[address] = input;
	}//write
};

class Line
{
private:
	int valid;
	int dirty;
	int tag; // LSB 14 bits used
	int accessCount;
  int age;	
  unsigned char * words;
public:

	Line()
	{
		valid = 0;
		dirty = 0;
		tag = 0;
		age = 0;
    accessCount = 0;
		words = new unsigned char[LINESIZE];
		for(int i = 0; i < LINESIZE; i++)
			words[i] = 0;
	}//Line
	
	~Line()
	{
		delete[] words;
	}//~Line

	void clearAccessCount()
	{
		accessCount = 0;
	}

	void clearAge()
	{
		age = 0;
	}

	void clearDirtyBit()
	{
		dirty = 0;
	}//clearDirtyBit

	void clearValidBit()
	{
		valid = 0;
	}//clearValidBit

	int computeNativeBlock()
	{
		return tag << 2;
	}//computeNativeBlock

	int getAccessCount()
	{
		return accessCount;
	}//getAccessCount

	int getAge()
	{
	return age;
	} 

	int getTag()
	{
		return tag;
	}//getTag

	void incAge()
	{
		age++;
	}//incAccessCount

	int isDirty()
	{
		return dirty;
	}//isDirty

	int isValid()
	{
		return valid;
	}//isValid

	unsigned char readLine(int offset)
	{
		return words[offset];
	}//readLine

	void redefine(int v, int d, int t, unsigned char const * w)
		{
			if(v == 0)
				valid = 0;
			else
				valid = 1;

			if(d == 0)
				dirty = 0;
			else
				dirty = 1;

			tag = t;

			for(int i = 0; i < LINESIZE; i++)
				words[i] = w[i];
	}//redefine

	void setDirtyBit()
	{
		dirty = 1;
	}//setDirtyBit

	void setValidBit()
	{
		valid = 1;
	}//setValidBit

	void writeLine(int offset, unsigned char data)
	{
		words[offset] = data;
	}//writeLine

};

class Set
{
private:
	Line * lines;
	int setNum;
public:
	Set(int setNumber)
	{
		setNum = setNumber;
		lines = new Line[NUMWAYS];
	}//Set()

	~Set()
	{
		delete[] lines;
	}//~Set()

	int getLRU()
	{
		int currAge = 0;
		int LRUIndex = 0;

		for(int i = 0; i < NUMWAYS; i++)
			if(lines[i].getAge() > currAge)
			{
				currAge = lines[i].getAge();
				LRUIndex = i;
			}//if

		return LRUIndex;
	}//getLRU

	Line * getLine(int lineNum)
	{
		return &(lines[lineNum]);
	}//getLine

	int getSetNum()
	{
		return setNum;
	}//getSetNum

	void incAge(int exception)
	{
		for(int i = 0; i < NUMWAYS; i++)
		{
			if(i != exception)
			{
				//cout << "Incrementing age of line " << i << endl;
				lines[i].incAge();
			}
		}
	}
};

class SACache
{
private:
	Set * sets[NUMSETS];
public:
	SACache()
	{
		for(int i = 0; i < NUMSETS; i++)
			sets[i] = new Set(i);
	}//SACache
	~SACache()
	{
		for(int i = 0; i < NUMSETS; i++)
			delete sets[i];
	}//~SACache

	Set * getSet(int s)
	{
		return sets[s];
	}//getSet

	void printCache()
	{
			cout << "\t\t\t\t\t\t===== THE CACHE =====" << endl;
			cout << "\t\t\t\t\t\toff: 0  1  2  3  d  LRU  TAG" << endl;
			cout << "\t\t\t\t\t\t     v  v  v  v  v  v    v"<< endl;
		for(int i = 0; i < NUMSETS; i++)
		{
			cout << "s " << i << endl;
			 for(int j = 0; j < NUMWAYS; j++)
			 {
			 	cout << "\t\tline " << j << ":    ";
			 	for(int k = 0; k < LINESIZE; k++)
			 		cout << " " << hex << uppercase << setw(2) << (int)sets[i]->getLine(j)->readLine(k);
			 	
			 	cout << "  " << hex << sets[i]->getLine(j)->isDirty();
			 	cout << "  " << dec << sets[i]->getLine(j)->getAge();
			 	cout << "    " << hex << (sets[i]->getLine(j)->getTag() << 2);
			 	cout << endl;
			 }//for
		}//for
			//cout << endl;
	}//printCache

	unsigned char read(int address, Memory & mem, int & hit, int & dirty)
	{
		int tag = 0;
		int setNum = 0;
		int offset = 0;

		factorAddress(address, tag, setNum, offset);

		Set * current = sets[setNum];

		bool h = false;
		int indexHit = 0;
		for(int i = 0; i < NUMWAYS; i++)
		{
			if(current->getLine(i)->getTag() == tag && current->getLine(i)->isValid())
			{
				h = true;
				indexHit = i;
				break;
			}//if
		}//for

		if(h)
		{
			hit = 1;
			dirty = current->getLine(indexHit)->isDirty();
			current->getLine(indexHit)->clearAge();
  			current->incAge(indexHit);	
			return current->getLine(indexHit)->readLine(offset);
		}//if

		else
		{
			hit = 0;
			int LRUIndex = current->getLRU();

			if(current->getLine(LRUIndex)->isDirty())
			{	
				//write out to memory
				for(int i = 0; i < LINESIZE; i++)
				{
					mem.writeMem(current->getLine(LRUIndex)->computeNativeBlock() + i, current->getLine(LRUIndex)->readLine(i));
				}//for
			}//if

			dirty = current->getLine(LRUIndex)->isDirty(); 

			current->getLine(LRUIndex)->clearDirtyBit();

			current->getLine(LRUIndex)->redefine(1, 0, tag, mem.getBlock(address));

			current->getLine(LRUIndex)->clearAge();
      		current->incAge(LRUIndex);

			return current->getLine(LRUIndex)->readLine(offset);
			//bring in memory to cache
		}//else

	}//read

	void write(int address, unsigned char data, Memory & mem)
	{

		int tag = 0;
		int setNum = 0;
		int offset = 0;

		factorAddress(address, tag, setNum, offset);

		Set * current = sets[setNum];

		//compare tag and vaild bits for a hit
		bool hit = false;
		int indexHit = 0;
		for(int i = 0; i < NUMWAYS; i++)
		{	
			if(current->getLine(i)->getTag() == tag && current->getLine(i)->isValid())
			{
				hit = true;
				indexHit = i;
				break;
			}//if
		}//for

		if(hit)
		{
			current->getLine(indexHit)->writeLine(offset, data);
			current->getLine(indexHit)->setDirtyBit();
			current->getLine(indexHit)->clearAge();
			current->incAge(indexHit);
		}//hit

		else // !hit
		{
			//evict the LRU

			int LRUIndex = current->getLRU();

			//if LRU is dirty, write it out to memory
			if(current->getLine(LRUIndex)->isDirty())
			{	
				//write out to memory
				for(int i = 0; i < LINESIZE; i++)
				{
					mem.writeMem(current->getLine(LRUIndex)->computeNativeBlock() + i, current->getLine(LRUIndex)->readLine(i));
				}//for
			}//if

			//write data into cache
			current->getLine(LRUIndex)->redefine(1, 1, tag, mem.getBlock(address));
			current->getLine(LRUIndex)->writeLine(offset, data);
			current->getLine(LRUIndex)->setDirtyBit();
			current->getLine(LRUIndex)->clearAge();

			current->incAge(LRUIndex);
		}//else

	}//write
	
};

int main(int argc, char const *argv[])
{
	string s;
	int address = 0;
	bool write = 0;
	unsigned char data = 0;

	unsigned char readData = 0;
	int hit = 0;
	int dirty = 0;

	Memory mem;
	SACache sac;
	ifstream iFile(argv[1]);
	ofstream output("sa-out.txt");
	if(iFile.is_open())
	{
		while(getline(iFile,s))
		{
			address = 0;
			write = 0;
			data = 0;
			processInput(s, address, write, data);

			if(write)
				sac.write(address, data, mem);
			else
			{
				readData = sac.read(address, mem, hit, dirty);
				unsigned char a = 0;
				writeBits(0,3,4,7,a,readData);
				output << hex << uppercase << (int)a;
				writeBits(0,3,0,3,a,readData);
				output << hex << uppercase << (int)a;
				output << " " << hit << " " << dirty << endl;
			}//else


		}//while
		output.close();
		iFile.close();
	}//if

	else
		cout << "Couldn't open file..." << endl;

	return 0;
}//main
