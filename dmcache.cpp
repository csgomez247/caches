//Nathan Lin, Corbin Gomez

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <string>
#include <fstream>

#define MEMSIZE 65536
#define NUMLINES 32
#define LINESIZE 8

using namespace std;

void newLine();
int unsigned charToHex(unsigned char c);
int extractBits(int a, int b, int n);
int extractBits(int a, int b, long n);
void factorAddress(int address, unsigned char & tag, int & setNum, int & offset);
void printB(unsigned long x);
void printB(int x);
void printB(unsigned char x);
void printH(int x);
void printData(unsigned char data);
void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, unsigned char from);
void processInput(string s, unsigned char & tag, int & setNum, int & offset, bool & write, unsigned char & data);
void setBit(unsigned long & data, int pos);
void clearBit(unsigned long & data, int pos);

class Line;
class DMCache;
class Memory;

void newLine()
{
	cout << endl;
}//newLine

int unsigned charToHex(unsigned char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else
		return c - 'A' + 10;
}//unsigned charToHex

int extractBits(int a, int b, int n)
{
	int r = 0;
	for(int i = a; i <= b; i++)
		r |= 1 << i;
	r &= n;
	r >>= a;
	return r;
}//extractBits

int extractBits(int a, int b, unsigned long n)
{
	int r = 0;
	for(int i = a; i <= b; i++)
		r |= 1 << i;
	r &= n;
	r >>= a;
	return r;
}//extractBits

void printB(unsigned long x)
{
	for(int i = 31; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printB(int x)
{
	for(int i = 15; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printB(unsigned char x)
{
	for(int i = 7; i >= 0; i--)
	{
		cout << extractBits(i,i,x);
	}//for
}//printBinary

void printH(int x)
{
	cout << uppercase << hex << x;
	cout << resetiosflags(ios::uppercase) << resetiosflags(ios::hex);
}

void printData(unsigned char data)
{
	unsigned char a = 0;
	writeBits(0,3,4,7,a,data);
	cout << hex << uppercase << (int)a;
	writeBits(0,3,0,3,a,data);
	cout << hex << uppercase << (int)a;
}//printData

void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, unsigned char from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void writeBits(int toA, int toB, int fromA, int fromB, int & to, unsigned char from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void writeBits(int toA, int toB, int fromA, int fromB, unsigned char & to, int from)
{

	if(toB - toA != fromB - fromA)
	{
		cout << EXIT_FAILURE << endl;
		exit(1);
	}//if

	int r = extractBits(fromA, fromB, from);

	for(int i = toA; i <= toB; i++)
	{
		//cout << "comping " << extractBits(i-toA, i-toA, r) << " to " << extractBits(i,i,to) << endl;
		if(extractBits(i-toA, i-toA, r) != extractBits(i,i,to))
		{
			if(extractBits(i-toA, i-toA, r) == 0)
			{
				to &= ~(1 << i);
				//cout << "to&: "; printB(to); cout << endl;
			}
			else
			{
				to |= extractBits(i-toA, i-toA, r) << i;
				//to |= 1 << i;
				//cout << "to|: "; printB(to); cout << endl;
			}
		}//if
		/*else
			{cout << "bbb: "; printB(to); cout << endl;}*/
	}//for
}//writeBits

void processInput(string s, int & address, bool & write, unsigned char & data)
{
	int input[10];
	for(int i = 0; i < 10; i++)
		input[i] = charToHex(s[i]);

	writeBits(0,3,0,3,address,input[3]);
	writeBits(4,7,0,3,address,input[2]);
	writeBits(8,11,0,3,address,input[1]);
	writeBits(12,15,0,3,address,input[0]);
	
	if(s[5] == 'F' && s[6] == 'F')
		write = true;
	else
		write = false;

	writeBits(4,7,0,3,data,input[8]);
	writeBits(0,3,0,3,data,input[9]);

}//processInput

void factorAddress(int address, unsigned char & tag, int & setNum, int & offset)
{
	//write tag
	writeBits(0, 7, 8, 15, tag, address);

	//write setNum
	writeBits(0, 4, 3, 7, setNum, address);

	//write offset
	writeBits(0, 2, 0, 2, offset, address);
}//factorAddress

void setBit(unsigned long & data, int pos)
{
	data |= 1 << pos;
}//setBit

void clearBit(unsigned long & data, int pos)
{
	data &= ~(1 << pos);
}//clearBit

class Memory
{
private:
	unsigned char * mem;
public:
	Memory()
	{
		mem = new unsigned char[MEMSIZE];
		for(int i = 0; i < MEMSIZE; i++)
			mem[i] = 0;
	}//Memory()

	~Memory()
	{
		delete [] mem;
	}//~Memory()

	int computeBlock(int address)
	{
		return (address / LINESIZE) * LINESIZE;
	}//computeBlock

	unsigned char * getBlock(int address)
	{
		return & mem[(address / LINESIZE) * LINESIZE];
	}//getBlock

	unsigned char * getMemory()
	{
		return mem;
	}//getMemory

	unsigned char readMem(int address)
	{
		return mem[address];
	}//read

	void writeMem(int address, unsigned char input)
	{
		mem[address] = input;
	}//write
};

class Line
{
private:
	unsigned char tag;
	unsigned char * words;
	int lineNum;

public:
	static unsigned long validBits;
	static unsigned long dirtyBits;

	Line(int i)
	{
		lineNum = i;
		tag = 0;
		words = new unsigned char[8];
		for(int i = 0; i < 8; i++)
			words[i] = 0;
	}//Line()
	~Line()
	{
		delete [] words;
	}//words

	void writeLine(int offset, unsigned char data)
	{
		words[offset] = data;
	}//writeLine

	int getAddress(int offset)
	{
		int address;

		//write the tag
		writeBits(8,15, 0,7, address, tag);

		//write the setNum
		writeBits(3,7, 0,4, address, (unsigned char)lineNum);

		//write the offset
		writeBits(0,2, 0,2, address, (unsigned char)offset);

		return address;
	}//getAddress

	int getAddressBlockIndex()
	{
		int address = 0;

		//write the tag
		writeBits(8,15, 0,7, address, tag);

		//write the setNum
		writeBits(3,7, 0,4, address, (unsigned char)lineNum);

		//write the offset
		//writeBits(0,2, 0,2, address, (unsigned char)offset);

		return (address / LINESIZE) * LINESIZE;
	}//getAddressBlockIndex

	unsigned char getData(int offset)
	{
		return words[offset]; 
	}//getData

	unsigned char getTag()
	{
		return tag;
	}//getTag

	int getLineNum()
	{
		return lineNum;
	}//getLineNum

	friend ostream& operator<<(ostream& os, const Line & obj)
	{
		cout << "Line " << obj.lineNum << ": " << endl;
		cout << "\tTag: "; printB(obj.tag); cout << endl;
		cout << endl;
		return os;
	}//operator<<

	int isDirty()
	{
		//return extractBits(lineNum,lineNum, dirtyBits);
		return (dirtyBits & (1 << lineNum)) >> lineNum;
	}//printDirtyBits

	int isValid()
	{
		//return extractBits(lineNum,lineNum, validBits);
		return (validBits & (1 << lineNum)) >> lineNum;
	}//isValid

	void setTag(unsigned char t)
	{
		tag = t;
	}//setTag;

	void redefine(int v, int d, unsigned char t, unsigned char const * w)
	{
		if(v == 0)
			clearBit(validBits, lineNum);
		else
			setBit(validBits, lineNum);

		if(d == 0)
			clearBit(dirtyBits, lineNum);
		else
			setBit(dirtyBits, lineNum);

		tag = t;

		for(int i = 0; i < 8; i++)
			words[i] = w[i];
	}//redefine

	void setDirtyBit()
	{
		setBit(dirtyBits, lineNum);
	}//setDirtyBit

	void setValidBit()
	{
		setBit(validBits, lineNum);
	}//setValidBit

	void clearDirtyBit()
	{
		clearBit(dirtyBits, lineNum);
	}//clearDirtyBit

	void clearValidBit()
	{
		clearBit(validBits, lineNum);
	}//clearValidBit

	bool tagMatches(unsigned char t)
	{
		return tag == t;
	}//tagMatches
}; // Line

class DMCache
{
private:
	Line * cacheLines[NUMLINES];
	unsigned long dirtyBits;
	unsigned long validBits;

public:
	DMCache()
	{
		for(int i = 0; i < NUMLINES; i++)
			cacheLines[i] = new Line(i);
	}//DMCache
	~DMCache()
	{
		for(int i = 0; i < NUMLINES; i++)
			delete cacheLines[i];
	}//~DMCache

	Line * getCacheLine(int lineNum)
	{
		return cacheLines[lineNum];
	}//getCacheLine

	int getCacheLineNum(int lineNum)
	{
		return cacheLines[lineNum]->getLineNum();
	}//getCacheLineNum

	void printDirtyBits()
	{
		for(int i = 0; i < NUMLINES; i++)
			cout << "l " << getCacheLineNum(i) << ":\t" << cacheLines[i]->isDirty() << endl;
	}//printDirtyBits

	void printLines()
	{
		for(int i = 0; i < NUMLINES; i++)
			cout << *(cacheLines[i]);
	}//printLines

	void printCache()
	{
			cout << "\t===== THE CACHE =====" << endl;
			cout << "\toff: 0  1  2  3  4  5  6  7" << endl;
			cout << "\t     v  v  v  v  v  v  v  v" << endl;
		for(int i = 0; i < NUMLINES; i++)
		{
			cout << "l " << dec << i << ">\t";
			for(int j = 0; j < LINESIZE; j++)
				cout << hex << uppercase << setw(2) << (int)cacheLines[i]->getData(j) << " ";
			
			cout << "----> v" << getCacheLine(i)->isValid() << " d" << getCacheLine(i)->isDirty() << " t" << setw(2) << setprecision(2) << hex << uppercase << (int)getCacheLine(i)->getTag();
			cout << endl; 
		}//for
			//cout << endl;
	}//printCache

	unsigned char readCache(int address, Memory & mem, int & hit, int & dirty)
	{
		unsigned char tag = 0;
		int setNum = 0;
		int offset = 0;

		factorAddress(address, tag, setNum, offset);

		//cout << "Reading mem[" << hex << uppercase << address << "]" << ", line " << dec << setNum << " at offset " << offset << " with tag " << hex << uppercase << (int)tag << endl;

		Line * current = getCacheLine(setNum);

		//find correct line based on mapping strategy

		//check tag bits for match / hit & check if it is valid
		if(current->tagMatches(tag) && current->isValid())
		{
			//cout << "Read hit!" << endl;
			hit = 1;
			dirty = current->isDirty();
			//cout << "Obtained " << hex << uppercase << (int)current->getData(offset) << " from line " << dec << setNum << " offset " << offset << endl;

			return current->getData(offset);
		}//if

		else // if(!current->tagMatches || )
		{
			//cout << "Read miss!" << endl;
			hit = 0;
			if (current->isDirty())
			{
				//cout << "Writing dirty line to mem[" << hex << uppercase << current->getAddressBlockIndex() << "]" << endl;
				//write to memory
				for(int i = 0; i < 8; i++)
				{
					mem.writeMem(i + current->getAddressBlockIndex(), current->getData(i));
					//cout << "mem[" << i + current->getAddressBlockIndex() << "] = " << hex << uppercase << (int)mem.readMem(i + current->getAddressBlockIndex()) << endl;
				}//for
			}//if

			dirty = current->isDirty();

			//cout << "Grabbing new line from mem[" << hex << uppercase << mem.computeBlock(address) << "]" << endl;
			current->redefine(1, 0, tag, mem.getBlock(address));

			//cout << "Obtained " << hex << uppercase << (int)current->getData(offset) << " from line " << dec << setNum << " offset " << offset << endl;
			return current->getData(offset);
		}//else
	}//read

	void writeCache(int address, unsigned char data, Memory & mem)
	{
		unsigned char tag = 0;
		int setNum = 0;
		int offset = 0;

		factorAddress(address, tag, setNum, offset);

		//cout << "Writing mem[" << hex << uppercase << address << "] = " << (int)data << " to line " << dec << setNum << " at offset " << offset << " with tag " << hex << uppercase << (int)tag << endl;

		Line * current = getCacheLine(setNum);

		//check tag bits for match / hit & check if it is valid
		if(!current->tagMatches(tag) || !current->isValid())
		{
			//cout << "Write miss!" << endl;

			if(current->isDirty())
			{
				//cout << "Writing dirty line to mem[" << hex << uppercase << current->getAddressBlockIndex() << "]" << endl;
				for(int i = 0; i < 8; i++)
				{
					mem.writeMem(i + current->getAddressBlockIndex(), current->getData(i));
					//cout << "mem[" << i + current->getAddressBlockIndex() << "] = " << hex << uppercase << (int)mem.readMem(i + current->getAddressBlockIndex()) << endl;
				}//for
			}//if

			//cout << "Grabbing new line from mem[" << hex << uppercase << mem.computeBlock(address) << "]" << endl;
			//bring it in from memory
			current->redefine(1, 0, tag, mem.getBlock(address));
		}//if
	
		// else
		// 	cout << "Write hit!" << endl;

		//cout << "Writing " << hex << uppercase << (int) data << " to line " << dec << setNum << " offset " << dec << offset << endl;
		current->writeLine(offset, data);

		if(!current->isDirty())
		{
			//cout << "Setting the dirty bit" << endl;
			current->setDirtyBit();
		}//if
	}//writeCache

};//DMCache

unsigned long Line::dirtyBits = 0;
unsigned long Line::validBits = 0;

int main(int argc, char const *argv[])
{
	string s;
	int address = 0;
	bool write = 0;
	unsigned char data = 0;

	unsigned char readData = 0;
	int hit = 0;
	int dirty = 0;

	Memory mem;
	DMCache dmc;
	ifstream iFile(argv[1]);
	ofstream output("dm-out.txt");
	if(iFile.is_open())
	{
		while(getline(iFile,s))
		{
			address = 0;
			write = 0;
			data = 0;
			processInput(s, address, write, data);
			
			// cout << "input: " << s << endl;
			// cout << "\taddress: " << hex << uppercase << address << endl;
			// cout << "\twrite:   " << write << endl;
			// cout << "\tdata:    " << hex << uppercase << (unsigned int)data << endl;

			//cout << "input: " << s << endl;

			if(write)
				dmc.writeCache(address, data, mem);
			else
			{
				readData = dmc.readCache(address, mem, hit, dirty);
				unsigned char a = 0;
				writeBits(0,3,4,7,a,readData);
				output << hex << uppercase << (int)a;
				writeBits(0,3,0,3,a,readData);
				output << hex << uppercase << (int)a;
				output << " " << hit << " " << dirty << endl;
			}//else

			//dmc.printCache();

		}//while
		output.close();
		iFile.close();
	}//if

	else
		cout << "Couldn't open file..." << endl;

	return 0;
}//main
